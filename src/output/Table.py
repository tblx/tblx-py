import output.csv_parser as csv
import output.printer as prt


class Table(list):
	"""Class for the table back-end."""

	def __init__(self, filepath: str):
		with open(filepath, mode="r", encoding="utf-8") as csv_file:
			super().__init__(csv.parser(csv_file.readlines()))
		self.header = self[0]
		self.data = self[1:]
		# Storing the width of each column.
		self.maxWidthColumn = []
		for i in range(len(self.header)):
			maxi = 0
			for row in self:
				if len(row[i]) > maxi:
					maxi = len(row[i])
			self.maxWidthColumn.append(maxi)
		
		self.print_mode:str = "all" # TODO : Enum


	def printHeader(self) -> str:
		"""
		Returns the rendered block of the header.
		"""
		return (prt.separator(self.maxWidthColumn, "top")
				+ "\n" + prt.data(self.maxWidthColumn, self.header)
				+ "\n" + prt.separator(self.maxWidthColumn, "bottom"))


	def printData(self) -> str:
		"""
		Returns the rendered block of the data.
		"""
		return (prt.separator(self.maxWidthColumn, "top")
				+ "\n" + "\n".join(prt.data(self.maxWidthColumn, self.data[i])
								   for i in range(len(self.data)))
				+ "\n" + prt.separator(self.maxWidthColumn, "bottom"))


	def printAllFloating(self) -> str:
		"""
		Returns the rendered blocks for the header and the data
		but with a gap between the two.
		"""

		# imo this should be a styling setting

		return self.printHeader() + "\n" + self.printData()


	def printAll(self) -> str:
		"""
		Returns the rendered blocks for the header and the data.
		"""
		return (prt.separator(self.maxWidthColumn, "top")
				+ "\n" + prt.data(self.maxWidthColumn, self.header)
				+ "\n" + prt.separator(self.maxWidthColumn, "middle") + "\n"
				+ "\n".join(prt.data(self.maxWidthColumn, self.data[i])
							for i in range(len(self.data)))
				+ "\n" + prt.separator(self.maxWidthColumn, "bottom"))


	def __repr__(self):
		# str.title() sure is convenient # tho i would've loved being able to use regex
		return eval(f"self.print{self.print_mode.title().replace(' ','')}()")

