def findSep(raw: [str]) -> str:
	"""
	Returns the character that serves as separator in the CSV.

	raw: list of containing the raw lines of the file.
	"""
	separators = ";,"
	for separator in separators:
		for i0, c0 in enumerate(raw[0]):
			if c0 == separator and len(raw) > 1:
				for i1, c1 in enumerate(raw[1]):
					if c1 == separator:
						return separator
			if c0 == separator and len(raw) == 1:
				return separator
	raise ValueError(f"Could not find the separator.")


def parser(raw: [str]) -> [str]:
	"""
	Returns the parsed list of lines where separators are removed
	and cell transformed into string.

	raw: list of containing the raw lines of the file.
	"""
	raw = [line.replace("\n", "") for line in raw]  # Remove line break.
	while "" in raw:  # Remove empty line.
		raw.remove("")
	separator = findSep(raw)  # Find the separator.
	parsed = []
	for k, line in enumerate(raw):
		parsed.append([])
		index = []
		# Finding separators to be removed.
		for i in range(len(line)):
			if line[i] == separator:
				index.append(i)
		# Removing separators.
		parsed[k].append(line[0:index[0]])
		for i in range(1, len(index)):
			parsed[k].append(line[index[i-1]+1:index[i]])
		# Adding empty cell to make sure each row has the same number of cell.
		max_length = max([len(row) for row in parsed])
		for index, row in enumerate(parsed):
			if len(row) < max_length:
				parsed[index] += ["" for i in range(max_length - len(row))]
	return parsed
