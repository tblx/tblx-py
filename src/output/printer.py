import os
import json

from tblx import PROJECT_PATH



# Importing the styling from a JSON file.
with open(f'{PROJECT_PATH}/data/style.json', encoding='utf-8') as STYLE_FILE:
	JSON_DATA = json.load(STYLE_FILE)
	STYLE_CONFIG = JSON_DATA["CONFIG"]
	STYLE = JSON_DATA["UNI"]
	LEN_PADDING = len(STYLE_CONFIG["padding"].expandtabs())


def textGroup(maxWidth: int, cell: str) -> str:
	"""
	Returns a string containing the cell string and sufficient padding.

	maxWidth: integer of the maximum width in the column.
	cell: string containing the cell data.
	"""
	return (STYLE_CONFIG["padding"] + cell + " " * (maxWidth - len(cell))
			+ STYLE_CONFIG["padding"])


def sepGroup(widthColumn: int) -> str:
	"""
	Returns a group of separator depending on the column width.

	widthColumn: integer of the maximum width in the column.
	"""
	return STYLE["HorSep"]*(2*LEN_PADDING + widthColumn)


def separator(maxWidthColumn: [int], position: str = "middle") -> str:
	"""
	Returns the string of a separating row depending of the position.

	maxWidthColumn: list of integer corresponding to each column maximum width.
	position: string of the position of the line: top, middle, bottom.
	"""
	if position not in ["top", "middle", "bottom"]:
		raise ValueError(
			f"Only top, middle, and bottom positions accepted. Not: {position}"
		)
	return (STYLE[position]["left"]
			+ STYLE[position]["middle"].join([
				sepGroup(width) for width in maxWidthColumn
			]) + STYLE[position]["right"])


def data(maxWidthColumn: [int], row: [str]) -> str:
	"""
	Returns the string of a row.

	maxWidthColumn: list of integer corresponding to each column maximum width.
	row: list of strings corresponding to the row.
	"""
	return (STYLE["VerSep"]
			+ STYLE["VerSep"].join([
				textGroup(maxWidthColumn[i], row[i])
				for i in range(len(row))
			]) + STYLE["VerSep"])
