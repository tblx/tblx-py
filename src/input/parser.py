import os
import sys
import json

from input.handler import Handler
from tblx import PROJECT_PATH

from collections import OrderedDict



class ArgumentParser:
	
	
	def __init__(self, arguments:str, source:dict=None):
		
		# by default, the cwd should be the root of the project's path ; i.e. tblx*/src/ # !! NMV LET'S NOT DO THAT (the python way it is...)
		# os.chdir(
		# 	# resourcesLocation != arguments[0] (when compiled) != this.__file__, supplied from main file, очень важный различие
		# 	os.path.dirname( os.path.abspath(resourcesLocation) )
		# ) # life saviour
		# print(os.getcwd(), __file__)
		# CWD should be conserved ; to safely access intrinsic resources, use PROJECT_PATH
		
		self.source = source
		
		if self.source is None:
			assert os.path.isfile(path:=f'{PROJECT_PATH}/data/input/args.json')
			
			self.source = json.load(open(path),
				object_pairs_hook=OrderedDict) # argument declaration order matterrrrs
		
		self.parse(arguments[1 : ])
		
		self.handle()
	
	
	def parse(self, raw:[str]) -> {'':any}:
		
		raw = sum([each.split('=') for each in raw], []) # expect sys.argv-like format
		
		args:dict = self.source.copy() # necessity ? TODO
		keymap = list(self.source.keys())
		
		for a,arg in enumerate(raw):
			
			# anonymous arguments
			if arg[0] != '-' and len(keymap) > 0:
				args[keymap[0]] = arg
				keymap.pop(0)
				continue
			
			# if there is no specified value then this is a flag
			# and its value equals its sheer presence
			attribute = True
			
			# stray values
			if a < len(raw) - 1 and raw[a+1][0] != '-':
				attribute = raw.pop(a+1)
			
			# compact flags (-abc)
			if arg[1].isalnum():
				for flag in arg[1 : ]:
					args[flag] = attribute
				continue
			
			# standard argument
			args[arg.strip('-')] = attribute
		
		self.args = args
		return args
		
		
	def handle(self):
		"""
		this is awful, kill me ; someone please rewrite this for the sake of my sanity
		
		nvm i did & it's quite something now i'm actually fond of just how clean this turned out
		"""
		
		Handler.__init__()

		## TODO : fallback call for empty args set # i.e. argumentless behaviour
		
		ignored_arguments:list = [] # consider a set instead ? order doesn't really matter much here
		
		for key,value in self.args.items():
			# much cleaner, i love this
			if not hasattr(Handler, key):
				ignored_arguments += [key]
				continue
			
			eval(f"Handler.{key}(value=value)") # do not substitute value for its evaluated counterpart
		
		Handler.__exit__()

		# you probably want to be able to see this past the table itself
		if (amount:=len(ignored_arguments)) > 0:
			print(f"Unknown arguments found ({amount}) : {ignored_arguments}")
		

