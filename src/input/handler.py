import os

from tblx import PROJECT_PATH



class Handler:
	'''
	stateless ; all methods are static functions
	
	The following void functions --should all return None-- and expect an initial dedicated guard clause,
	as they are all called so long as they match an argument in args.json regardless of the value supplied upon calling ;
	as that should be handled by the function itself, not upstream.
	'''
	

	# i know.
	def __init__(): # desperation
		global TABLE
		TABLE = None
	
	def __exit__(): # this is a cry for help i am so done with this
		print(TABLE)

	
	@staticmethod
	def help(value=False):
		if not value: return; # yes i know, this is python ; still, idc, signifies clear halting to me
		# leave me be, had we truly wanted rigour, we wouldn't have picked this language to begin with
		
		# eventually feed this into custom pager
		for line in open(filepath:=f'{PROJECT_PATH}/data/help.txt').readlines():
			print(line, end="")
		quit()
		
	
	@staticmethod
	def install(value=False):
		if not value: return;
		
		## TODO : auto install
		
		
	
	@staticmethod
	def filename(value=''):
		if not (value != '' and os.path.isfile(os.path.normpath(value))): return;
		
		from output.Table import Table # given the structure of the file, this is fine,
		# and actually makes a lot of sense given how this is a dynamic implementation
		# wherein any function may be substituted for on the fly (do not judge the usefulness of such an implementation)
		
		global TABLE
		TABLE = Table(filepath=value)

	
	# now under normal circumstances i would've most definitely dried the following declarations buuuuut i just couldn't be bothered soooo
	# also that wasn't my idea and i still think it's a terrible one, even past the awfully wet implementation

	@staticmethod
	def header(value=False):
		if not value: return;

		TABLE.print_mode = "header"
	
	@staticmethod
	def data(value=False):
		if not value: return;

		TABLE.print_mode = "data"

	@staticmethod
	def floating(value=False):
		if not value: return;

		TABLE.print_mode = "all floating" # this isn't even consistent kill me
		
