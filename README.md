# tblx-py[^1]

Unicode CLI CSV previewer in Python

```
╭────────────────────────────╮
│                            │
│                            │
│          _     _           │
│     _   |:|   |:|          │
│    |:|_ |:| _ |:|_   _     │
│    |:::)|:||:\|:(:\ /:)    │
│    |:|__|:|_):):|):X:(     │
│     \:::)::::/|:(:/ \:)    │
│                            │
│                            │
│                            │
╰────────────────────────────╯
```

## Dependencies

- `python3`

No dependencies are required for installation on Windows.



## Usage

See `man tblx` for an in-depth description of the script's behaviour.
For a quick list of available options, call `tblx` by itself.

Piping the result into a pager such as `less` (or `more` on DOS) is heavily recommended.
Alternatively, one may simply choose to save the returned preview into a file.

> [!NOTE]
> The manpage has not been written as of yet.



## Installation 

**Supplying the script with the `--install` flag will trigger an OS dependant installation sequence.**

Beware, as changing the path to a directory that is not already part of your system's PATH variable will forcefully add the `bin/` directory of the provided path to it, in order for the script to be callable nonetheless.

No matter how and most importantly where you choose to install the script, so long as its `bin/` entry resides in the PATH, the script will be accessible.


### Windows

It is recommended to copy the build into either its own subfolder entry in `%programfiles%`, or in a local folder such as `%userprofile%\.bin\`.
Alternatively, one may make use of the aforementioned `--install` flag for an automatic installation.


### POSIX (Linux, BSD, MacOS, etc)

Either invoke the script with the `--install` flag, or simply copy the python file to /usr/local/bin or /usr/bin/. The former is likely to be preferred.

There is no explicit need to compile anything, as it will automatically call your local python installation, assuming it is installed, and that it supports all the features the script makes use of. As such, unless your python installation is regularly updated (ideally >=3.9), you will likely want to compile the file, or use one of the precompiled [releases](https://codeberg.org/tblx/tblx-py/releases/latest/).

> [!WARNING]
> Precompiled releases will not be compatible with platforms other than Windows, Linux and FreeBSD; and architecture other than x86-64.



## Compile yourself

The compilation process is fairly straightforward due to its lack of particular requirements. As for any other standard python script, any "compiler" (bundler) may be used.

> [!TIP]
> Prefer [PyInstaller](https://pyinstaller.org), as it is the one precompiled binaries are released thanks to.
> On POSIX systems, we recommend the use of the system package manager or pipx over pip to install `pyinstaller`.

> [!INFORMATION]
> The compiled binaries will be generated in `tblx-py/dist/`.

### Windows

```batch
git clone https://codeberg.org/tblx/tblx-py
cd tblx-py\
pyinstaller src\tblx.py -F --add-data="data;data"
```


### POSIX (Linux, BSD, MacOS, etc)

> [!CAUTION]
> The Makefile is not functional yet. Do not use those commands yet.

```shell
git clone https://codeberg.org/tblx/tblx-py
cd tblx-py
make run
```

If you want to install it on your system:

```shell
make install
```
If you don't want to use `make`:


```shell
pyinstaller src/tblx.py -F --add-data="data;data"
```


[^1]: Pronounced `/tblks/`. Yes, we know.
